/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a1861840
 */
public class Pratica {
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        System.out.println("Sistema Operacional: "+System.getProperty("os.name"));
        System.out.println("Nº de processadores: "+rt.availableProcessors());
        System.out.println("Memória total: "+rt.totalMemory()/1048576+" MB");
        System.out.println("Memória livre: "+rt.freeMemory()/1048576+" MB");
        System.out.println("Máximo de memoria usada: "+rt.maxMemory()/1048576+" MB");
    }
}